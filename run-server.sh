#!/bin/sh
echo "Init git submodules"
git submodule update --init

if [ ! -d "reveal.js" ]; then
  echo >&2 "submodule was not initialized";
  exit 1;
fi

echo "Copy Demos theme assets"
cp -r custom/themes/fonts reveal.js/dist/theme
cp -r custom/themes/images reveal.js/dist/theme
cp custom/themes/*.scss reveal.js/css/theme/source

echo "Copy presentation.html into reveal.js directory"
sed 's%reveal.js/%%g' presentation.html > reveal.js/index.html

(
    cd reveal.js || exit 1;
    echo "Create symlink for images"
    test -L images || ln -s ../images ./
)

echo "Build and start live server"
if ! command -v npm 2>&1 /dev/null
then
    echo "npm command not found, try fallback using docker"
    command -v docker >/dev/null 2>&1 || \
        { echo >&2 "No npm or docker found. Can not do anything"; exit 1; }
    docker run -it --rm --user "$(id -u)":"$(id -g)" \
        -v "$(pwd):/reveal.js" -w /reveal.js -p 8000:8000 node:10-slim \
        bash -c "cd reveal.js && npm install && npm run build && npm start"
else
    # run commands in subshell, because changing of directory
    (
    cd reveal.js || exit 1;
    npm install
    npm run build
    npm start
    )
fi
